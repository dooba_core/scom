/* Dooba SDK
 * USB Serial Communication
 */

#ifndef	__SCOM_TERM_H
#define	__SCOM_TERM_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Buffer Size
#ifndef	SCOM_TERM_RXBUF_SIZE
#define	SCOM_TERM_RXBUF_SIZE								128
#endif

// Sequence Buffer Size
#define	SCOM_TERM_SEQBUF_SIZE								5

// History Size (Entries)
#ifndef	SCOM_TERM_HIST_SIZE
#define	SCOM_TERM_HIST_SIZE									5
#endif

// History Entry Structure
struct scom_term_hist_entry
{
	// String
	char s[SCOM_TERM_RXBUF_SIZE];
	uint8_t l;
};

// Initialize
extern void scom_term_init(uint8_t (*rx_callback)(char *x, uint8_t s));

// Set Special Character Callback
extern void scom_term_set_special_cb(void (*special_callback)(uint8_t x, char *buf, uint8_t s));

// Update
extern void scom_term_update();

// Print Format String
extern void scom_term_printf(char *fmt, ...);

// Print Format String - va_list
extern void scom_term_vprintf(char *fmt, va_list args);

// Format String Printer
extern void scom_term_printf_print(void *user, uint8_t c);

// Send String
extern void scom_term_print(char *s);

// Send
extern void scom_term_tx(uint8_t *d, uint8_t s);

// Send Byte
extern void scom_term_write(uint8_t d);

// Serial Comm RX Handler
extern void scom_term_rx(uint8_t x);

// Handle Backspace
extern void scom_term_bksp();

// Handle Delete
extern void scom_term_del();

// Handle Up
extern void scom_term_up();

// Handle Down
extern void scom_term_down();

// Handle Left
extern void scom_term_left();

// Handle Right
extern void scom_term_right();

// Handle Home
extern void scom_term_home();

// Handle End
extern void scom_term_end();

// Handle Normal Character
extern void scom_term_char(uint8_t c);

// Erase line
extern void scom_term_erase_line();

// Apply line
extern void scom_term_apply_line(char *s, uint8_t l);

// Push history entry
extern void scom_term_hist_push(char *s, uint8_t l);

// Get history entry
extern struct scom_term_hist_entry *scom_term_hist_get(uint8_t idx);

#endif
