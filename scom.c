/* Dooba SDK
 * USB Serial Communication
 */

// External Includes
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>

// Internal Includes
#include "scom.h"

// Receive Buffer
uint8_t *scom_rxbuf;
uint8_t scom_rxbuf_size;
uint8_t scom_rxbuf_pos;

// RX Callback
void (*scom_rx_callback)(uint8_t x);

// Overflow Callback
void (*scom_overflow_callback)(uint8_t x);

// Initialize
void scom_init(uint8_t *rxbuf, uint8_t rxbuf_size, void (*rx_callback)(uint8_t x), void (*overflow_callback)(uint8_t x))
{
	// Set RX Buffer
	scom_rxbuf = rxbuf;
	scom_rxbuf_size = rxbuf_size;
	scom_rxbuf_pos = 0;

	// Set Callbacks
	scom_rx_callback = rx_callback;
	scom_overflow_callback = overflow_callback;

	// Enable Power
	#ifdef PRR0
	PRR0 &= ~(1 << PRUSART0);
	#endif

	// Load up Prescaler
	UBRR0H = (SCOM_PRESCALE >> 8);
	UBRR0L = SCOM_PRESCALE & 0xff;

	// Enable UART RX & TX
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);
	UCSR0C = (3 << UCSZ00);

	// Enable RX Interrupt
	UCSR0B |= (1 << RXCIE0);
}

// Send Byte
void scom_write(uint8_t d)
{
	// Wait for TX Ready
	while((UCSR0A & (1 << UDRE0)) == 0);

	// TX
	UDR0 = d;
}

// Send
void scom_send(uint8_t *d, uint8_t s)
{
	uint8_t i;

	// Loop
	for(i = 0; i < s; i = i + 1)
	{
		// Wait for TX Ready
		while((UCSR0A & (1 << UDRE0)) == 0);

		// TX
		UDR0 = d[i];
	}
}

// Consume Input (Free up RX Buffer space)
void scom_consume(uint8_t s)
{
	uint8_t i;

	// Check Size
	if(s == 0)											{ return; }

	// Lock Interrupts
	cli();

	// Slice RX Buffer
	if(s > scom_rxbuf_pos)								{ s = scom_rxbuf_pos; }
	for(i = 0; i < (scom_rxbuf_pos - s); i = i + 1)		{ scom_rxbuf[i] = scom_rxbuf[s + i]; }

	// Update Position
	scom_rxbuf_pos = scom_rxbuf_pos - s;

	// Release Interrupts
	sei();
}

// UART RX Interrupt Service Routine
#ifdef USART0_RX_vect
ISR(USART0_RX_vect)
#else
ISR(USART_RX_vect)
#endif
{
	uint8_t x;

	// Pull Byte from UART & Clear RXC Flag
	x = UDR0;

	// Push into RX Buffer
	if(scom_rxbuf_pos < scom_rxbuf_size)				{ scom_rxbuf[scom_rxbuf_pos] = x; scom_rxbuf_pos = scom_rxbuf_pos + 1; }
	else												{ if(scom_overflow_callback) { scom_overflow_callback(x); } return; }

	// Notify RX Callback
	if(scom_rx_callback)								{ scom_rx_callback(x); }
}
