/* Dooba SDK
 * USB Serial Communication
 */

#ifndef	__SCOM_H
#define	__SCOM_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Define Defaults
#ifndef	SCOM_BAUDRATE
#define	SCOM_BAUDRATE				19200UL
#endif

// UART Prescaler
#define	SCOM_PRESCALE				((F_CPU / (SCOM_BAUDRATE * 16UL)) - 1)

// Receive Buffer
extern uint8_t *scom_rxbuf;
extern uint8_t scom_rxbuf_size;
extern uint8_t scom_rxbuf_pos;

// RX Callback
extern void (*scom_rx_callback)(uint8_t x);

// Overflow Callback
extern void (*scom_overflow_callback)(uint8_t x);

// Initialize
extern void scom_init(uint8_t *rxbuf, uint8_t rxbuf_size, void (*rx_callback)(uint8_t x), void (*overflow_callback)(uint8_t x));

// Send Byte
extern void scom_write(uint8_t d);

// Send
extern void scom_send(uint8_t *d, uint8_t s);

// Consume Input (Free up RX Buffer space)
extern void scom_consume(uint8_t s);

#endif
