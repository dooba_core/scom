/* Dooba SDK
 * USB Serial Communication
 */

// External Includes
#include <string.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/cprintf.h>

// Internal Includes
#include "term.h"
#include "scom.h"

// RX Buffer
uint8_t scom_term_rxbuf[SCOM_TERM_RXBUF_SIZE];

// Cursor Position
uint8_t scom_term_csr;

// Sequence Buffer
uint8_t scom_term_seqbuf[SCOM_TERM_SEQBUF_SIZE];
uint8_t scom_term_seqlen;

// History
struct scom_term_hist_entry scom_term_hist_entries[SCOM_TERM_HIST_SIZE];
struct scom_term_hist_entry *scom_term_hist[SCOM_TERM_HIST_SIZE];
uint8_t scom_term_hist_size;
uint8_t scom_term_hist_csr;

// Message Size
uint8_t scom_term_msg_size;

// RX Callback
uint8_t (*scom_term_rx_callback)(char *x, uint8_t s);

// Initialize
void scom_term_init(uint8_t (*rx_callback)(char *x, uint8_t s))
{
	uint8_t i;

	// Set Handler
	scom_term_rx_callback = rx_callback;

	// Clear Message & Cursor
	scom_term_msg_size = 0;
	scom_term_csr = 0;

	// Clear Sequence
	scom_term_seqlen = 0;

	// Clear History
	for(i = 0; i < SCOM_TERM_HIST_SIZE; i = i + 1)																{ scom_term_hist_entries[i].l = 0; scom_term_hist[i] = &(scom_term_hist_entries[i]); }
	scom_term_hist_size = 0;
	scom_term_hist_csr = 0;

	// Initialize Serial Communication
	scom_init(scom_term_rxbuf, SCOM_TERM_RXBUF_SIZE, scom_term_rx, 0);
}

// Update
void scom_term_update()
{
	uint8_t rsize;
	uint8_t c = 0;

	// Check Message
	if(scom_term_msg_size == 0)																					{ return; }

	// Check Real Size
	rsize = scom_term_msg_size;
	while((rsize) && ((scom_rxbuf[rsize - 1] == '\r') || (scom_rxbuf[rsize - 1] == '\n')))						{ rsize = rsize - 1; }

	// Call handler (size - 1 because we don't want to pass the newline)
	if(scom_term_rx_callback)																					{ c = scom_term_rx_callback((char *)scom_rxbuf, rsize); }

	// Consume Message (unless handler consumed it)
	if(c == 0)																									{ scom_consume(scom_term_msg_size); }

	// Clear everything
	scom_term_msg_size = 0;
	scom_term_hist_csr = 0;
	scom_term_csr = 0;
}

// Print Format String
void scom_term_printf(char *fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, fmt);

	// Print
	scom_term_vprintf(fmt, ap);

	// Release Args
	va_end(ap);
}

// Print Format String - va_list
void scom_term_vprintf(char *fmt, va_list args)
{
	// Print
	cvprintf(scom_term_printf_print, 0, fmt, args);
}

// Format String Printer
void scom_term_printf_print(void *user, uint8_t c)
{
	// Write
	scom_term_write(c);
}

// Send String
void scom_term_print(char *s)
{
	// Send String
	scom_term_tx((uint8_t *)s, strlen((char *)s));
}

// Send
void scom_term_tx(uint8_t *d, uint8_t s)
{
	// Send
	for(; s > 0; s--)																							{ scom_term_write(*d++); }
}

// Send Byte
void scom_term_write(uint8_t d)
{
	// Precede every \n with a \r
	if(d == '\n')																							{ scom_write('\r'); }

	// Write Character
	scom_write(d);
}

// Serial Comm RX Handler
void scom_term_rx(uint8_t x)
{
	// Pull back character from buffer for intelligent processing
	scom_rxbuf_pos = scom_rxbuf_pos - 1;

	// Don't accept anything until buffer is ready
	if(scom_term_msg_size)																						{ return; }

	// Handle Escape Sequences
	if(scom_term_seqlen)
	{
		if(scom_term_seqlen >= SCOM_TERM_SEQBUF_SIZE)															{ scom_term_seqlen = 0; return; }
		scom_term_seqbuf[scom_term_seqlen] = x;
		scom_term_seqlen = scom_term_seqlen + 1;
		if((scom_term_seqlen == 3) && (scom_term_seqbuf[1] == '[') && (scom_term_seqbuf[2] == 'A'))				{ scom_term_up(); scom_term_seqlen = 0; }
		else if((scom_term_seqlen == 3) && (scom_term_seqbuf[1] == '[') && (scom_term_seqbuf[2] == 'B'))		{ scom_term_down(); scom_term_seqlen = 0; }
		else if((scom_term_seqlen == 3) && (scom_term_seqbuf[1] == '[') && (scom_term_seqbuf[2] == 'C'))		{ scom_term_right(); scom_term_seqlen = 0; }
		else if((scom_term_seqlen == 3) && (scom_term_seqbuf[1] == '[') && (scom_term_seqbuf[2] == 'D'))		{ scom_term_left(); scom_term_seqlen = 0; }
		else if((scom_term_seqlen == 3) && (scom_term_seqbuf[1] == '[') && (scom_term_seqbuf[2] == 'F'))		{ scom_term_end(); scom_term_seqlen = 0; }
		else if((scom_term_seqlen == 3) && (scom_term_seqbuf[1] == '[') && (scom_term_seqbuf[2] == 'H'))		{ scom_term_home(); scom_term_seqlen = 0; }
		else if((scom_term_seqlen == 4) && (scom_term_seqbuf[1] == '[') && (scom_term_seqbuf[2] == '1'))		{ scom_term_home(); scom_term_seqlen = 0; }
		else if((scom_term_seqlen == 4) && (scom_term_seqbuf[1] == '[') && (scom_term_seqbuf[2] == '4'))		{ scom_term_end(); scom_term_seqlen = 0; }
		else if((scom_term_seqlen == 4) && (scom_term_seqbuf[1] == '[') && (scom_term_seqbuf[2] == '3'))		{ scom_term_del(); scom_term_seqlen = 0; }
		else																									{ /* NoOp */ }
		return;
	}

	// Handle Input
	if(x == 0x1b)																								{ scom_term_seqbuf[0] = x; scom_term_seqlen = 1; }
	else if((x == '\b') || (x == 0x7f))																			{ scom_term_bksp(); }
	else if((x == '\r') || (x == '\n'))																			{ scom_term_msg_size = scom_rxbuf_pos; }
	else																										{ scom_term_char(x); }
}

// Handle Backspace
void scom_term_bksp()
{
	uint8_t i;

	// Check Cursor Position
	if(scom_term_csr == 0)																						{ return; }

	// Go Back
	scom_term_csr = scom_term_csr - 1;
	scom_rxbuf_pos = scom_rxbuf_pos - 1;
	scom_term_print("\b\x1b[s");							// Backspace and save cursor position
	for(i = scom_term_csr; i < scom_rxbuf_pos; i = i + 1)														{ scom_term_rxbuf[i] = scom_term_rxbuf[i + 1]; scom_write(scom_term_rxbuf[i]); }
	scom_term_print(" \x1b[u");								// Clear char and restore cursor position
}

// Handle Delete
void scom_term_del()
{
	uint8_t i;

	// Check Cursor
	if(scom_term_csr >= scom_rxbuf_pos)																			{ return; }

	// Delete
	scom_rxbuf_pos = scom_rxbuf_pos - 1;
	scom_term_print("\x1b[s");								// Save cursor position
	for(i = scom_term_csr; i < scom_rxbuf_pos; i = i + 1)														{ scom_term_rxbuf[i] = scom_term_rxbuf[i + 1]; scom_write(scom_term_rxbuf[i]); }
	scom_term_print(" \x1b[u");								// Clear char and restore cursor position
}

// Handle Up
void scom_term_up()
{
	struct scom_term_hist_entry *e;

	// Inc history cursor
	if(scom_term_hist_csr >= scom_term_hist_size)																{ return; }
	scom_term_hist_csr = scom_term_hist_csr + 1;

	// Get entry
	e = scom_term_hist_get(scom_term_hist_csr - 1);
	if(e == 0)																									{ return; }

	// Erase current line & Apply history
	scom_term_erase_line();
	scom_term_apply_line(e->s, e->l);
}

// Handle Down
void scom_term_down()
{
	struct scom_term_hist_entry *e;

	// Dec history cursor
	if(scom_term_hist_csr == 0)																					{ return; }
	scom_term_hist_csr = scom_term_hist_csr - 1;

	// Erase current line
	scom_term_erase_line();

	// Check cursor
	if(scom_term_hist_csr == 0)																					{ return; }

	// Get entry
	e = scom_term_hist_get(scom_term_hist_csr - 1);
	if(e == 0)																									{ return; }

	// Apply history
	scom_term_apply_line(e->s, e->l);
}

// Handle Left
void scom_term_left()
{
	// Check Cursor
	if(scom_term_csr == 0)																						{ return; }

	// Move Cursor
	scom_term_csr = scom_term_csr - 1;

	// Move on terminal
	scom_term_print("\x1b[D");
}

// Handle Right
void scom_term_right()
{
	// Check Cursor
	if(scom_term_csr >= scom_rxbuf_pos)																			{ return; }

	// Move Cursor
	scom_term_csr = scom_term_csr + 1;

	// Move on terminal
	scom_term_print("\x1b[C");
}

// Handle Home
void scom_term_home()
{
	// Move to home
	while(scom_term_csr)																						{ scom_term_print("\x1b[D"); scom_term_csr = scom_term_csr - 1; }
}

// Handle End
void scom_term_end()
{
	// Move to end
	while(scom_term_csr < scom_rxbuf_pos)																		{ scom_term_print("\x1b[C"); scom_term_csr = scom_term_csr + 1; }
}

// Handle Normal Character
void scom_term_char(uint8_t c)
{
	uint8_t i;

	// Check Position
	if(scom_rxbuf_pos >= scom_rxbuf_size)																		{ return; }

	// Add Character
	scom_term_printf("%c\x1b[s", c);						// Print char and save cursor position
	for(i = scom_term_csr; i < scom_rxbuf_pos; i = i + 1)														{ scom_write(scom_term_rxbuf[i]); }
	for(i = scom_rxbuf_pos; i > scom_term_csr; i = i - 1)														{ scom_term_rxbuf[i] = scom_term_rxbuf[i - 1]; }
	scom_term_print("\x1b[u");								// Restore cursor position
	scom_term_rxbuf[scom_term_csr] = c;
	scom_term_csr = scom_term_csr + 1;
	scom_rxbuf_pos = scom_rxbuf_pos + 1;
}

// Erase line
void scom_term_erase_line()
{
	uint8_t i;

	// Home
	scom_term_home();

	// Save cursor
	scom_term_print("\x1b[s");

	// Clear Line
	for(i = 0; i < scom_rxbuf_pos; i = i + 1)																	{ scom_write(' '); }

	// Restore cursor
	scom_term_print("\x1b[u");

	// Clear Line
	scom_rxbuf_pos = 0;
	scom_term_csr = 0;
}

// Apply line
void scom_term_apply_line(char *s, uint8_t l)
{
	uint8_t i;

	// Apply Line
	for(i = 0; i < l; i = i + 1)																				{ scom_write(s[i]); }
	memcpy(scom_term_rxbuf, s, l);
	scom_rxbuf_pos = l;
	scom_term_csr = l;
}

// Push history entry
void scom_term_hist_push(char *s, uint8_t l)
{
	struct scom_term_hist_entry *e;
	uint8_t i;

	// Limit Length
	if(l > SCOM_TERM_RXBUF_SIZE)																				{ l = SCOM_TERM_RXBUF_SIZE; }

	// Don't repeat history
	e = scom_term_hist_get(0);
	if((e) && (e->l == l))																						{ if(memcmp(s, e->s, l) == 0) { return; } }

	// Set entry
	e = scom_term_hist[SCOM_TERM_HIST_SIZE - 1];
	memcpy(e->s, s, l);
	e->l = l;

	// Move history
	for(i = (SCOM_TERM_HIST_SIZE - 1); i > 0; i = i - 1)														{ scom_term_hist[i] = scom_term_hist[i - 1]; }
	scom_term_hist[0] = e;

	// Set size
	if(scom_term_hist_size < SCOM_TERM_HIST_SIZE)																{ scom_term_hist_size = scom_term_hist_size + 1; }
}

// Get history entry
struct scom_term_hist_entry *scom_term_hist_get(uint8_t idx)
{
	// Get
	if(scom_term_hist_size == 0)																				{ return 0; }
	if(idx >= scom_term_hist_size)																				{ idx = scom_term_hist_size - 1; }
	return scom_term_hist[idx];
}
